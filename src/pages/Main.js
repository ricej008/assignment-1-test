import { useState } from 'react'
import { useForm } from "react-hook-form";

import TimerDisplay from '../components/TimerDisplay';


function Main() {
    const [list, setList] = useState([])

    const { handleSubmit, register } = useForm()

    const onSubmit = (values) => {
        console.log(values)
        setList((currentState) => {
            return [...currentState, values]
        })
    }

    return (
        <main className="container mx-auto my-5 border">
            <section>
                <TimerDisplay value="3:45" />
            </section>
            <section>
                <form className="flex space-x-1" onSubmit={handleSubmit(onSubmit)}>
                    <input className="bg-gray-300 p-2 rounded" type="text" name="name" ref={register} />
                    <input className="bg-gray-300 p-2 rounded" type="text" name="minutes" ref={register} />
                    <button className="bg-green-300 p-2 rounded" type="submit">Add</button>
                </form>
            </section>
            <section>
                <ul>
                    {list.map((value, index) => (
                        <li key={index}>
                            <input className="bg-gray-300 p-2 rounded" value={value.name} />
                        </li>
                    ))}
                </ul>
            </section>
        </main>
    )
}

export default Main