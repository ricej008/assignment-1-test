
function TimerDisplay(props) {
    return (
        <div className="text-4xl bg-green-500 p-2 text-center">
            <p>
                {props.value}
            </p>
        </div>
    );
}

export default TimerDisplay