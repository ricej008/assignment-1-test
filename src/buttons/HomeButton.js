import { FaBeer } from 'react-icons/fa';

function HomeButton(props) {
    return (
        <button className="bg-yellow-300 p-2" type="submit">
            <h1 className="text-3xl"><FaBeer />TimeBlok</h1>
        </button>
    );
}

export default HomeButton