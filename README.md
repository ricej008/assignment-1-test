## Group 13 - COMP2035

Private gitlab repository [https://gitlab.com/ostc-group-13/assignment-1](https://gitlab.com/ostc-group-13/assignment-1)

| Group Member | UniSA username | gitlab username |
| -------------| -------------- | --------------- |
| Matthew Thomson    | @thomj016   | @thomj016   |
| Danika King    | @kinde001   | @kinde001   |
| Ethan Rickaby    | @ricej008   | @ricej008   |


## Welcome to the Group 13 Readme!
Our aplication will be a web app. Programming languages used include HTML, CSS, JS and ReactJS. <br />
The feature added by Ethan is: <br />
The feature added by Matthew is: <br />
The feature added by Danika is: <br />

### Installation
Installation is not required.

# TimeBlock


## About TimeBlock
TimeBlok is a responsive modern web application for tracking time blocks.

### Show Current
This would be about how to view the interface...

### Add New
Adding a new Blok can be done as follows...

### Edit Block
Amending an existing Blok, changing the time, or description

### Delete Block
Remove a Blok - warning...

### Delete All Blocks
Remove all of the Blok's - VERY CAREFUL


This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
